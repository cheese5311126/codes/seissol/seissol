# SeisSol

[![SQAaaS badge shields.io](https://img.shields.io/badge/sqaaas%20software-silver-silver)](https://eu.badgr.com/public/assertions/V1IRTJ4DSZessZ71umM-YQ "SQAaaS silver badge achieved")
[![GitLab Release (latest by SemVer)](https://img.shields.io/github/v/release/SeisSol/SeisSol)](https://github.com/SeisSol/SeisSol/releases)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.11405512.svg)](https://doi.org/10.5281/zenodo.11405512)



SeisSol is a high-performance computational seismology software that focuses on the simulation of complex earthquake
scenarios. It supports various rheologies (elastic, anisotropic-elastic, acoustic, viscoelastic, poroelastic), boundary
conditions and dynamic rupture laws. SeisSol uses high-order discontinuous Galerkin discretisation with an Arbitrary
DERivative (ADER) local time stepping on unstructured adaptive tetrahedral meshes.

## Documentation
For further information, please visit the SeisSol 
[Official Web Site](https://seissol.org/).

## Developers
SeisSol is developed and maintained by the **Ludwigs-Maximilians-Universität (LMU, Munich)** and the **Technical University of Munich (TUM).**

List of developers:
1. Dr. Alice-Agnes Gabriel
2. Dr. Thmoas Ulrich 
3. Dr. Duo Li
4. Sara Aniko Wirp
5. Nico Schliwa
6. Sebastian Wolf
7. David Schneller
8. Vikas Kurapati

## License and copyright

BSD 3-Clause License

Copyright (c) 2019, SeisSol Group
Copyright (c) 2023, Intel Corporation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.